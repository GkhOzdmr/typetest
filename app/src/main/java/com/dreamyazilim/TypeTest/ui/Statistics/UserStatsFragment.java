package com.dreamyazilim.TypeTest.ui.Statistics;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.data.AppDatabase;
import com.dreamyazilim.TypeTest.data.model.WpmRecordModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class UserStatsFragment extends Fragment {

    private static final String TAG = "UserStatsFragment";

    private CompositeDisposable disposable = new CompositeDisposable();

    private Unbinder unbinder;
    @BindView(R.id.statsChart) LineChart statsChart;
    @BindView(R.id.tViewHighest) TextView tViewHighest;
    @BindView(R.id.tViewAverage) TextView tViewAverage;
    @BindView(R.id.tViewTestTaken) TextView tViewTestTaken;
    @BindView(R.id.tViewTotalPoint) TextView tViewTotalPoint;
    @BindString(R.string.highest_score) String highest_score;
    @BindString(R.string.average_score) String average_score;
    @BindString(R.string.total_tests_taken) String total_tests_taken;
    @BindString(R.string.total_points) String total_points;

    private List<Entry> entries = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_stats, container, false);
        unbinder = ButterKnife.bind(this,view);
        //Codes here
        updateUi();
        // /// /// // ///
        return view;
    }

    private void updateUi(){
        //Update Line Chart with recent Wpm scores
        getStats();

        AppDatabase appDatabase = AppDatabase.getAppDatabase(getContext());
        disposable.add(
                Observable.just(appDatabase)
                        .observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                            int highestWpm = appDatabase.wpmRecordDAO().getHighestWpm();
                            int wpmAverage = appDatabase.wpmRecordDAO().getWpmAverage();
                            int totalTests = appDatabase.wpmRecordDAO().getTotalTests();
                            int totalPoints = appDatabase.wpmRecordDAO().getTotalPoints();
                            tViewHighest.setText(String.format(highest_score+" "+highestWpm));
                            tViewAverage.setText(String.format(average_score+" "+wpmAverage));
                            tViewTestTaken.setText(String.format(total_tests_taken+" "+totalTests));
                            tViewTotalPoint.setText(String.format(total_points+" "+totalPoints));
                })
        );

    }

    private void getStats() {

        AppDatabase appDatabase = AppDatabase.getAppDatabase(getContext());
        List<WpmRecordModel> records = appDatabase.wpmRecordDAO().getAllRecords();
        disposable.add(
                Observable.just(appDatabase)
                        .observeOn(Schedulers.io())
                        .subscribe(s -> {

                            for(WpmRecordModel data: records) {
                                entries.add(new Entry(data.getId(),data.getCurrentWpmRecord()));
                                Log.d(TAG, "getStats: " +
                                        "id: " + data.getId() +
                                        " wpm: " + data.getCurrentWpmRecord() +
                                        " date: " + data.getDate() +
                                        " point: " + data.getPointsEarned());
                            }
                            if(entries.size()>0){
                                LineDataSet setStats = new LineDataSet(entries,"Word Per Minute");
                                setStats.setDrawFilled(true);
                                setStats.setFillColor(Color.WHITE);
                                setStats.setValueTextColor(Color.WHITE);
                                setStats.setValueTextSize(12);
                                List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                                dataSets.add(setStats);
                                LineData data = new LineData(dataSets);
                                statsChart.setData(data);
                                statsChart.invalidate();
                            }


                        })

        );
        Log.d(TAG, "getStats: ");
    }

    @OnClick(R.id.buttonBack)
    public void onButtonBackClick(){
        getActivity().onBackPressed();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        disposable.clear();
    }
}
