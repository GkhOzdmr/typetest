package com.dreamyazilim.TypeTest.ui.Statistics;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.data.items.StatisticsItems;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class StatisticsActivity extends DaggerAppCompatActivity implements StatisticsContract.View  {

    @Inject
    StatisticsContract.Presenter statisticsPresenter;

    @BindView(R.id.recyclerViewItems) RecyclerView recyclerViewItems;

    private List<StatisticsItems> statisticsItems = new ArrayList<>();
    private StatisticsAdapter statisticsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        ButterKnife.bind(this);
         buildRecyclerView();

    }

    @OnClick(R.id.buttonBack)
    public void onButtonBackClick() {
        super.onBackPressed();
    }

    public void buildRecyclerView(){
        statisticsAdapter = new StatisticsAdapter(statisticsItems);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewItems.setLayoutManager(mLayoutManager);
        recyclerViewItems.setItemAnimator(new DefaultItemAnimator());
        recyclerViewItems.setAdapter(statisticsAdapter);

        StatisticsItems leaderBoard = new StatisticsItems(
                R.drawable.leaderboard,
                getResources().getString(R.string.leaderboard_item_title),
                getResources().getString(R.string.leaderboard_item_description));
        statisticsItems.add(leaderBoard);


        StatisticsItems userStat = new StatisticsItems(
                R.drawable.profile_placeholder, //TODO GET USERS PROFILE PICTURE
                getResources().getString(R.string.user_item_title),
                getResources().getString(R.string.user_item_description));
        statisticsItems.add(userStat);
        statisticsAdapter.notifyDataSetChanged();

        statisticsAdapter.setOnItemClickListener(position -> {
            //Fragment selectedFragment = null;
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (position) {
                case 0:
                    fragmentTransaction.replace(R.id.fragmentContainer,new LeaderboardFragment());
                    break;
                case 1:
                    fragmentTransaction.replace(R.id.fragmentContainer,new UserStatsFragment());
                    break;
            }
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });
    }
}
