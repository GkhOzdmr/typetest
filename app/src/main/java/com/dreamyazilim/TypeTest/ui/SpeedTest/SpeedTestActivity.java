package com.dreamyazilim.TypeTest.ui.SpeedTest;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.data.AppDatabase;
import com.dreamyazilim.TypeTest.data.model.WpmRecordModel;
import com.dreamyazilim.TypeTest.ui.Statistics.LeaderboardFragment;
import com.dreamyazilim.TypeTest.ui.Statistics.UserStatsFragment;
import com.dreamyazilim.TypeTest.util.ViewUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


/*TODO Make TextSwitch area Vector Drawable, +1 -1 animations for correct or incorrect words,*/

public class SpeedTestActivity extends DaggerAppCompatActivity implements SpeedTestContractor.View {

    @Inject
    SpeedTestContractor.Presenter speedTestPresenter;
    @Inject
    ViewUtils viewUtils;
    @Inject
    WpmCalculator wpmCalculator;

    //CONSTANTS
    private static final String TAG = "SpeedTestActivity";
    private static final int ANIMATION_TIME = 450;

    //VIEWS
    @BindView(R.id.tViewPrevWord) TextView tViewPrevWord;
    @BindView(R.id.tViewCurrentWord) TextView tViewCurrentWord;
    @BindView(R.id.tViewNextWord) TextView tViewNextWord;
    @BindView(R.id.tViewWpm) TextView tViewWpm;
    @BindView(R.id.tViewRwpm) TextView tViewRwpm;
    @BindView(R.id.tViewIncorrectWords) TextView tViewIncorrectWords;
    @BindView(R.id.tViewTimer) TextView tViewTimer;
    @BindView(R.id.editTextEnterWord) EditText editTextEnterWord;
    @BindView(R.id.buttonReset) Button buttonReset;
    @BindView(R.id.adViewBanner) AdView adViewBanner;
    @BindView(R.id.spinnerLanguage) Spinner spinnerLanguage;

    //DIMENSIONS
    @BindDimen(R.dimen.textview_small_height) int textview_small_height;
    @BindDimen(R.dimen.textview_small_width) int textview_small_width;
    @BindDimen(R.dimen.textview_big_height) int textview_big_height;
    @BindDimen(R.dimen.textview_big_width) int textview_big_width;

    private TextView[] textViewSwitcher = new TextView[3];
    private List<String> top200List;
    private int switchPosition = 1;
    private boolean startTimer = false;
    private String wordToCheck;

    private int countMe=0;
    private int countMe2=0;

    private InterstitialAd mInterstitialAd;

    Date d = new Date();
    private CharSequence dateNow=DateFormat.format("yyyy-MM-dd hh:mm:ss", d.getTime());

    private CompositeDisposable disposable = new CompositeDisposable();

    private CountDownTimer countDownTimer;//DON'T DELETE!

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speed_test);
        ButterKnife.bind(this);
        //Initialize
        onAttach();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onDetach();
    }

    @Override
    public void onAttach() {
        Log.d(TAG, "onAttach: "+dateNow);
        //Create array to provide to PresenterImpl
        top200List = Arrays.asList(getBaseContext().getResources().getStringArray(R.array.top_200));
        //Initializing TextView array
        textViewSwitcher[0] = tViewPrevWord;
        textViewSwitcher[1] = tViewCurrentWord;
        textViewSwitcher[2] = tViewNextWord;
        //Initializing other things
        speedTestPresenter.initialize();
        updateUi();
        //Placing view objects
        tViewPrevWord.setX((int) (viewUtils.screenOnePercent() * 25) - tViewPrevWord.getLayoutParams().width);
        tViewCurrentWord.setX((int) (viewUtils.screenOnePercent() * 50) - (tViewCurrentWord.getLayoutParams().width / 2));
        tViewNextWord.setX(viewUtils.screenOnePercent() * 75);
        editTextEnterWord.addTextChangedListener(textWatcher);
        resetButtonClick();
        //TODO add product flavor
        showAds();
        changeLanguage();
        Log.d(TAG, "onAttach: screen density: "+viewUtils.screenDensity());
    }


    public void changeLanguage() {
        spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        top200List = Arrays.asList(getBaseContext().getResources().getStringArray(R.array.top_200_english));
                        speedTestPresenter.updateArray();
                        updateUi();
                        break;
                    case 1:
                        top200List = Arrays.asList(getBaseContext().getResources().getStringArray(R.array.top_200_turkish));
                        speedTestPresenter.updateArray();
                        updateUi();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onDetach() {
        disposable.clear();
    }



    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!startTimer &&
                    editTextEnterWord.getText().toString().length()>0) {
                startTimer = true;
                spinnerLanguage.setEnabled(false);
                startCountDown();
                Log.d(TAG, "textChange: inside if: starTimer: "+startTimer);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (    editTextEnterWord.isEnabled()
                    && editTextEnterWord.getText().toString().contains(" ")
                    && !editTextEnterWord.getText().toString().equals("")) {

                wordToCheck = "";
                wordToCheck = editTextEnterWord.getText().toString().replaceAll("\\s+", "");
                speedTestPresenter.setWordToCheck(wordToCheck);
                viewSwitcher(textViewSwitcher);
                editTextEnterWord.setText("");
                speedTestPresenter.updateWords();

            }
        }
    };

    public void resetButtonClick() {
        disposable.add(
                RxView.clicks(buttonReset)
                        .subscribe(e -> {
                            editTextEnterWord.setEnabled(true);
                            if(countDownTimer!=null){ countDownTimer.cancel(); }
                            startTimer = false;
                            spinnerLanguage.setEnabled(true);
                            tViewTimer.setText("01:00");
                            editTextEnterWord.setHint(getResources().getString(R.string.enter_word_hint_tap));

                            updateUi();

                            switchPosition = 1;
                            positionZero(textViewSwitcher[0]);
                            positionOne(textViewSwitcher[1]);
                            positionTwo(textViewSwitcher[2]);
                            
                            wpmCalculator.resetCalculations();

                        })
        );
    }

    @Override
    public void onBackPressed() {
        disposable.add(
                Observable.just(1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();

                            }
                            super.onBackPressed();
                        })
        );

    }

    @OnClick(R.id.buttonBack)
    public void onButtonBackClick() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            super.onBackPressed();
        }
    }

    @OnClick(R.id.buttonNavMenu)
    public void onButtonNavMenuClick(){
        //Todo add nav menu
    }

    @OnClick(R.id.buttonStats)
    public void onButtonStatsClick(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer,new UserStatsFragment());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void startCountDown() {
        disposable.add(
                Observable.just(startTimer)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(e -> {
                                    Log.d(TAG, "startCountDown: startTimer: " + startTimer);
                                    if (startTimer) {
                                        Log.d(TAG, "countDownTimer:before: startTimer is: " + startTimer);
                                        countDownTimer = new CountDownTimer(60000, 1000) {
                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                tViewTimer.setText("00:" + millisUntilFinished / 1000);
                                                //display optimization for last 10 seconds
                                                if (millisUntilFinished <= 10000) {
                                                    tViewTimer.setText("00:0" + millisUntilFinished / 1000);
                                                }
                                            }

                                            @Override
                                            public void onFinish() {
                                                //This is to hide keyboard after timer stops
                                                View mView = getCurrentFocus();
                                                if (mView != null) {
                                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                    imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
                                                }
                                                spinnerLanguage.setEnabled(true);
                                                editTextEnterWord.setText("");
                                                editTextEnterWord.setEnabled(false);
                                                startTimer = false;
                                                editTextEnterWord.setHint(getResources().getString(R.string.enter_word_hint_reset));
                                                setResults();
                                                
                                            }
                                        }.start();
                                    }
                                }
                        )
        );
    }

    @Override
    public void viewSwitcher(TextView[] textViewsArray) {
        switch (switchPosition) {
            case 1:
                Log.d(TAG, "switchPosition == 1: " + switchPosition);
                positionZero(textViewSwitcher[0]);
                positionOne(textViewSwitcher[1]);
                positionTwo(textViewSwitcher[2]);
                switchPosition = 2;
                break;
            case 2:
                Log.d(TAG, "switchPosition == 2: " + switchPosition);
                positionZero(textViewSwitcher[1]);
                positionOne(textViewSwitcher[2]);
                positionTwo(textViewSwitcher[0]);
                switchPosition = 3;
                break;
            case 3:
                Log.d(TAG, "switchPosition == 3: " + switchPosition);
                positionZero(textViewSwitcher[2]);
                positionOne(textViewSwitcher[0]);
                positionTwo(textViewSwitcher[1]);
                switchPosition = 1;
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setResults() {
        Log.d(
                TAG, "setResults: correctWord: " + wpmCalculator.getCorrectWord()
                        + " incorrectWord: " + wpmCalculator.getIncorrectWord()
        );

        AppDatabase appDatabase = AppDatabase.getAppDatabase(this);

        //TODO switch room to rxjava maybe?

        tViewWpm.setText(Integer.toString(wpmCalculator.calculateWpm()));
        tViewRwpm.setText("");
        tViewIncorrectWords.setText(Integer.toString(wpmCalculator.getIncorrectWord()));
        WpmRecordModel wpmRecordModel = new WpmRecordModel();
        wpmRecordModel.setDate(dateNow.toString());
        wpmRecordModel.setCurrentWpmRecord(wpmCalculator.calculateWpm());
        wpmRecordModel.setPointsEarned(
                PointsCalculator.calculatePointsEarned(wpmCalculator.calculateWpm())
        );
        appDatabase.wpmRecordDAO().insertAll(wpmRecordModel);

    }

    @Override
    public List<String> getArrayList() {
        return top200List;
    }

    @Override
    public String getWordToCheck() {
        return wordToCheck;
    }

    @Override
    public void positionZero(final TextView textView) {
        Log.d(TAG, "startToEnd() called with: textView = [" + textView + "]");

        ObjectAnimator startToEnd = ObjectAnimator.ofFloat(textView,"translationX",
                viewUtils.screenOnePercent()*110,
                (viewUtils.screenOnePercent()*75));
        startToEnd.setDuration(ANIMATION_TIME/2);

        ObjectAnimator startToEndFirst = ObjectAnimator.ofFloat(textView,"translationX",0);
        startToEndFirst.setDuration(ANIMATION_TIME/2);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.play(startToEnd).after(startToEndFirst);
        animatorSet.start();

        startToEnd.addUpdateListener(animation -> {
                    textView.setTextColor(Color.BLUE);
                    textView.setText(speedTestPresenter.getNewWord());
                }
        );
    }

    @Override
    public void positionOne(final TextView textView) {
        Log.d(TAG, "centerToStart() called with: textView = [" + textView + "]");
        if(speedTestPresenter.isWordsMatching()){
            wpmCalculator.increaseCorrectWord();
            textView.setTextColor(Color.GREEN);

        }else{
            wpmCalculator.increaseIncorrectWord();
            textView.setTextColor(Color.RED);
        }
        ObjectAnimator centerToStart = ObjectAnimator.ofFloat(textView,"translationX",
                ((viewUtils.screenOnePercent()*50) - (textview_big_width/2)),
                ((viewUtils.screenOnePercent()*25) - (textview_small_width)) );

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(ANIMATION_TIME);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.playTogether(centerToStart);
        animatorSet.start();

        centerToStart.addUpdateListener(animation -> {
            RelativeLayout.LayoutParams params;
            params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.width = (textview_small_width);
            params.height = (textview_small_height);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            textView.setLayoutParams(params);
            textView.setTextSize(viewUtils.textSizeSetterSmall());

            textView.setText(speedTestPresenter.getOldWord());

        });
    }

    @Override
    public void positionTwo(final TextView textView) {
        Log.d(TAG, "endToCenter() called with: textView = [" + textView + "]");
        ObjectAnimator endToCenter = ObjectAnimator.ofFloat(textView,"translationX",
                (viewUtils.screenOnePercent()*75),
                ((viewUtils.screenOnePercent()*50) - (textview_big_width/2)));
        endToCenter.setDuration(ANIMATION_TIME);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.play(endToCenter);
        animatorSet.start();

        endToCenter.addUpdateListener(animation -> {
            RelativeLayout.LayoutParams params;
            params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.width = (textview_big_width);
            params.height = (textview_big_height);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            textView.setLayoutParams(params);
            textView.setTextSize(viewUtils.textSizeSetterBig());

            textView.setTextColor(Color.BLACK);
            textView.setText(speedTestPresenter.getCurrentWord());

        });
    }

    public void showAds() {
        disposable.add(
                Observable.just(1)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                            //Todo change ids before publication
                            MobileAds.initialize(this,
                                    "ca-app-pub-1610312558419875~6578892972");
                            mInterstitialAd = new InterstitialAd(this);
                            mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
                            mInterstitialAd.loadAd(new AdRequest.Builder().build());
                            adViewBanner.loadAd(new AdRequest.Builder().build());
                            mInterstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                                }
                            });
                        })
        );
    }

    @Override
    public void updateUi() {
        speedTestPresenter.updateWords();
        speedTestPresenter.updateWords();
        tViewPrevWord.setText("");
        tViewCurrentWord.setText(speedTestPresenter.getCurrentWord());
        tViewNextWord.setText(speedTestPresenter.getNewWord());
    }

    @Override
    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
