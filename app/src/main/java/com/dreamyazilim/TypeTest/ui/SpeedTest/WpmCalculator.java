package com.dreamyazilim.TypeTest.ui.SpeedTest;

import io.reactivex.Observable;

public class WpmCalculator {

    private int correctWord;
    private int incorrectWord;

    public WpmCalculator(){

    }

    public void increaseCorrectWord(){
        correctWord += 1;
    }

    public void increaseIncorrectWord(){
        incorrectWord += 1;
    }

    public int getCorrectWord() {
        return correctWord;
    }

    public int getIncorrectWord() {
        return incorrectWord;
    }

    public void resetCalculations(){
        correctWord=0;
        incorrectWord=0;
    }

    public int calculateWpm(){
        int calculatedWpm = correctWord-incorrectWord;
        return calculatedWpm;
    }

    public Observable<Integer> wpmObservable =
            Observable.just(correctWord,incorrectWord)
            .map(wpm -> wpm=correctWord-incorrectWord);

}
