package com.dreamyazilim.TypeTest.ui.Register;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dreamyazilim.TypeTest.R;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class RegisterActivity extends DaggerAppCompatActivity implements RegisterContract.View {

    private static final String TAG = "RegisterActivity";

    @Inject
    RegisterContract.Presenter registerPresenter;

    @BindView(R.id.relativeLayoutRegister) RelativeLayout relativeLayoutRegister;
    @BindView(R.id.buttonRegister) Button buttonRegister;
    @BindView(R.id.editTextEmail) EditText editTextEmail;
    @BindView(R.id.editTextPassword) EditText editTextPassword;
    @BindView(R.id.editTextPasswordConfirm) EditText editTextPasswordConfirm;
    @BindString(R.string.passwords_not_match) String passwords_not_match;
    @BindString(R.string.fields_empty) String fields_empty;

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate: ");
        runFadeInAnimation();
    }


    @OnClick(R.id.buttonRegister)
    public void buttonRegisterOnClick() {
         String email = editTextEmail.getText().toString(),
                password = editTextPassword.getText().toString(),
                confirmPassword = editTextPasswordConfirm.getText().toString();
        Log.d(TAG, "buttonRegisterOnClick: "
                + email +" "
                + password+" "
                + confirmPassword);

        //Checks if the passwords match
        if (password.equals(confirmPassword)) {
            Log.d(TAG, "buttonRegisterOnClick: succesfull");
            showProgressDialog(getResources().getString(R.string.please_wait));
            registerPresenter.attemptRegistration(email,password);
        }else{
            Log.d(TAG, "buttonRegisterOnClick: unsuccesfull");
            editTextPassword.setError(passwords_not_match);
            editTextPasswordConfirm.setError(passwords_not_match);
            showToast(getResources().getString(R.string.passwords_not_match));
        }
    }

    @OnClick(R.id.buttonBack)
    public void onButtonBackClick(){
        super.onBackPressed();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //Fadeout animation for screen changes
    private void runFadeInAnimation() {
        //fade animation
        Animation fadeAnim = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        fadeAnim.reset();
        //specify the layout to implement animation here
        relativeLayoutRegister.clearAnimation();
        relativeLayoutRegister.startAnimation(fadeAnim);
    }

    //Navigates to specified activity
    @Override
    public void navigateToActivity(Class<?> cls) {
        Context context = getBaseContext();
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
        finish();
    }

    //Show toast with message
    @Override
    public void showToast(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    //Checks if the fields are empty
    @Override
    public boolean checkFields() {
        String email = editTextEmail.getText().toString(),
                password = editTextPassword.getText().toString(),
                passwordConfirm = editTextPasswordConfirm.getText().toString();
        // Check for a valid password, if the user entered one.
        if (email.equals("") && password.equals("") && passwordConfirm.equals("")) {
            return false;
        }
        return true;
    }

    @Override
    public void showProgressDialog(String message) {
        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogStyleGreen);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void hideProgressDialog(boolean isSucceeded) {
        if (isSucceeded){
            mProgressDialog.hide();
            mProgressDialog.dismiss();
        }else{
            mProgressDialog.hide();
            showToast(getResources().getString(R.string.went_wrong));
            mProgressDialog.dismiss();
        }

    }

    @Override
    public void isVerificationSent(boolean isSent) {
        if(isSent){showToast(getResources().getString(R.string.email_sent));}
        else{showToast(getResources().getString(R.string.email_not_sent));}
    }


}
