package com.dreamyazilim.TypeTest.ui.Statistics;

import com.dreamyazilim.TypeTest.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class StatisticsActivityModule {

    @ActivityScoped
    @Provides
    static StatisticsContract.Presenter provideStatisticsPresenter(StatisticsContract.View statisticsView){
        return new StatisticsPresenterImpl(statisticsView);
    }

    @ActivityScoped
    @Binds
    abstract StatisticsContract.View provideStatisticsView(StatisticsActivity statisticsActivity);
}
