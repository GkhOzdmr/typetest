package com.dreamyazilim.TypeTest.ui.UserDetails;

import android.telecom.Call;
import android.util.Log;

import com.dreamyazilim.TypeTest.data.model.UserDetailsModel;
import com.dreamyazilim.TypeTest.data.remote.FirebaseApi;
import com.dreamyazilim.TypeTest.di.ActivityScoped;
import com.dreamyazilim.TypeTest.ui.Login.LoginPresenterImpl;
import com.dreamyazilim.TypeTest.ui.Main.MainActivity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;

@ActivityScoped
public class UserDetailsPresenterImpl implements UserDetailsContract.Presenter {

    private static final String TAG = "UserDetailsPresenterImp";

    private UserDetailsContract.View view;
    private Retrofit retrofit;

    private String idToken;
    private String localId;

    private CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    public UserDetailsPresenterImpl(UserDetailsContract.View view, Retrofit retrofit) {
        this.view = view;
        this.retrofit = retrofit;
    }

    @Override
    public void onDestroy() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    @Override
    public void uploadImage() {//Todo we are gonna need idToken to set details and localId to set and get imageUrl
        Log.d(TAG, "uploadImage: idToken: " + idToken + " localId: " + localId);
        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);
    }

    @Override
    public String getImageLink() {

        String imageUrl = "";
        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);

        return imageUrl;
    }

    @Override
    public void checkIfUserNameExists(String displayName, String photoUrl) {

        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);

        firebaseApi.checkIfUsernameExists("\"details/displayName\"", "\"" + displayName + "\"")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<UserDetailsModel.UserDetails>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onSuccess(Response<UserDetailsModel.UserDetails> response) {

                        UserDetailsModel.UserDetails.Details details = new UserDetailsModel.UserDetails.Details(displayName,photoUrl);

                        Log.d(TAG, "onNext of checkIfUserNameExists:" +
                                " localId " + response.body().getLocalId() +
                                " details " + response.body().getDetails());

                        for(UserDetailsModel.UserDetails.Details detils:response.body().getDetails()){
                            Log.d(TAG, "details: "+detils.getDisplayName()+
                                    " photoUrl "+detils.getPhotoUrl());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            Log.d(TAG, "onError of checkIfUserNameExists:" +
                                    " code: " + ((HttpException) e).code() +
                                    " message: " + ((HttpException) e).message() +
                                    " response: " + ((HttpException) e).response() +
                                    " localMessage " + e.getLocalizedMessage());
                        } else {
                            Log.d(TAG, "onError of checkIfUserNameExists(nonHttp):" +
                                    " message: " + e.getMessage() +
                                    " localizedMessage: " + e.getLocalizedMessage() +
                                    " cause: " + e.getCause());
                        }
                    }
                });
    }

    @Override
    public void setUserDetails(String displayName, String photoUrl) {
        Log.d(TAG, "setUserDetails: localId: " + LoginPresenterImpl.localId);
        view.showProggressDialog("Processing");

        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);
        List<UserDetailsModel.UserDetails.Details> details = (List<UserDetailsModel.UserDetails.Details>) new UserDetailsModel.UserDetails.Details(displayName,photoUrl);
        UserDetailsModel.UserDetails userDetails = new UserDetailsModel.UserDetails(LoginPresenterImpl.localId, details);

        firebaseApi.setUserDetails(LoginPresenterImpl.localId, userDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<UserDetailsModel.UserDetails>() {
                    @Override
                    public void onNext(UserDetailsModel.UserDetails userDetails) {
                        view.showToast(1, null);
                        view.hideProggressDialog();
                        Log.d(TAG, "onNext: ");
                        view.navigateToActivity(MainActivity.class);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError of setUserDetails e: " + e.getStackTrace());
                        if (e instanceof HttpException) {
                            HttpException exception = (HttpException) e;
                            Log.d(
                                    TAG, "onError: of setUserDetails" +
                                            "code:" + ((HttpException) e).code()
                                            + " message " + ((HttpException) e).message()
                                            + " response " + (((HttpException) e).response())
                            );
                        }
                        view.showToast(2, null);
                        view.hideProggressDialog();
                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });

    }
}
