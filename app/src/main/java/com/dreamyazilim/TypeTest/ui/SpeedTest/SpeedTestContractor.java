package com.dreamyazilim.TypeTest.ui.SpeedTest;

import android.widget.TextView;

import java.util.List;

public interface SpeedTestContractor {

     interface View{
        void onAttach();
        void onDetach();
        void showToast(String message);
        void positionZero(TextView textView);
        void positionOne(TextView textView);
        void positionTwo(TextView textView);
        void viewSwitcher(TextView[] textViewsArray);
        void setResults();
        void updateUi();
        List<String> getArrayList();
        String getWordToCheck();
    }

    interface Presenter{
        void initialize();
        void updateArray();
        String getNewWord();
        String getCurrentWord();
        String getOldWord();
        boolean isWordsMatching();
        void updateWords();
        void setWordToCheck(String wordToCheck);
        void setCurrentRecord(String date,int wpm,int points);
    }
}
