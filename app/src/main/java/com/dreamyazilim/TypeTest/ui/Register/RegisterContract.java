package com.dreamyazilim.TypeTest.ui.Register;

public interface RegisterContract {

    interface View{

        void navigateToActivity(Class<?> cls);

        void showToast(String message);

        boolean checkFields();

        void showProgressDialog(String message);

        void hideProgressDialog(boolean isSucceeded);

        void isVerificationSent(boolean isSent);

    }

    interface Presenter{

        void attemptRegistration(String userEmail, String userPassword);

        boolean isEmailValid(String userEmail);

        void sendEmailVerification(String idToken);

    }
}
