package com.dreamyazilim.TypeTest.ui.SpeedTest;

import android.util.Log;

import com.dreamyazilim.TypeTest.di.ActivityScoped;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

@ActivityScoped
public class SpeedTestPresenterImpl implements SpeedTestContractor.Presenter{

    private static final String TAG = "SpeedTestPresenterImpl";

    private SpeedTestContractor.View view;

    private String oldWord;
    private String currentWord;
    private String nextWord;
    private String wordToCheck;
    private List<String> top200List;
    public boolean wordsMatching;

    @Inject
    public SpeedTestPresenterImpl(SpeedTestContractor.View view){
        this.view=view;

    }

    @Override
    public void initialize() {
        updateArray();
       // updateWords();
    }

    @Override
    public void updateWords(){
        isWordsMatching();
        Random random = new Random();
        String randomFromArray = top200List.get(random.nextInt(top200List.size()));
        oldWord = currentWord;
        currentWord = nextWord;
        nextWord = randomFromArray;
        Log.d(TAG, "getNewWord(): oldWord: "+oldWord +" currentWord: "+currentWord+" nextWord: "+nextWord);
    }

    @Override
    public String getNewWord() {
        return nextWord;
    }

    @Override
    public String getCurrentWord() {
        return currentWord;
    }

    @Override
    public String getOldWord() {
        return oldWord;
    }

    @Override
    public void setWordToCheck(String wordToCheck) {
        this.wordToCheck = wordToCheck;
    }

    @Override
    public void setCurrentRecord(String date, int wpm, int points) {
        //todo do database writing here
        /*WpmRecordModel recordModel = new WpmRecordModel();
        recordModel.setDate(date);
        recordModel.setCurrentWpmRecord(wpm);
        recordModel.setPointsEarned(points);*/
    }

    @Override
    public boolean isWordsMatching(){
        if (currentWord != null && wordToCheck != null && wordToCheck.equals(currentWord)) {
            Log.d(TAG, "isWordsMatching(): wordToCheck: "+ wordToCheck);
            Log.d(TAG, "isWordsMatching(): currentWord: "+ currentWord);
            wordsMatching = true;
            this.wordToCheck = "";
        }else{
            Log.d(TAG, "isWordsMatching(): wordToCheck: "+ wordToCheck);
            Log.d(TAG, "isWordsMatching(): currentWord: "+ currentWord);
            wordsMatching = false;
            this.wordToCheck = "";
        }
        Log.d(
                TAG, "isWordsMatching() returned: " + wordsMatching
        );
        return wordsMatching;
    }

    @Override
    public void updateArray() {
        top200List = view.getArrayList();
    }
}
