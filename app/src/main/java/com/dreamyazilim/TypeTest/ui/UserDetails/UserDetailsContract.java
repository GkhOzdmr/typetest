package com.dreamyazilim.TypeTest.ui.UserDetails;

import android.content.Intent;
import android.support.annotation.Nullable;

public interface UserDetailsContract {

    interface View {
        void showToast(int displayMessage, @Nullable String message);

        void showProggressDialog(String message);

        void hideProggressDialog();

        void navigateToActivity(Class<?> cls);

        void getImage();

        void setImage(int resultCode, int requestCode, Intent data);
    }

    interface Presenter {
        void onDestroy();

        void uploadImage();

        String getImageLink();

        void checkIfUserNameExists(String displayName, String photoUrl);

        void setUserDetails(String displayName, String photoUrl);
    }

}
