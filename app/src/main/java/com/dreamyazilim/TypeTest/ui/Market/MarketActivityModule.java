package com.dreamyazilim.TypeTest.ui.Market;

import com.dreamyazilim.TypeTest.di.ActivityScoped;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class MarketActivityModule {

    @ActivityScoped
    @Provides
    static MarketContract.Presenter provideMarketPresenter(MarketContract.View marketView){
        return new MarketPresenterImpl(marketView);
    }

    @ActivityScoped
    @Binds
    abstract MarketContract.View provideMarketView(MarketActivity marketActivity);
}
