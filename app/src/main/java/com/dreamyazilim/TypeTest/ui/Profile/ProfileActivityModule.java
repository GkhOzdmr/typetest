package com.dreamyazilim.TypeTest.ui.Profile;

import com.dreamyazilim.TypeTest.data.BuildConfig;
import com.dreamyazilim.TypeTest.di.ActivityScoped;
import com.squareup.moshi.Moshi;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public abstract class ProfileActivityModule {

    @ActivityScoped
    @Provides
    static ProfileContract.Presenter providePresenter(ProfileContract.View profileView, Retrofit retrofit){
        return new ProfilePresenterImpl(profileView,retrofit);
    }

    @ActivityScoped
    @Binds
    abstract ProfileContract.View provideView(ProfileActivity profileActivity);

    @ActivityScoped
    @Provides
    static Retrofit provideRetrofit(Moshi moshi){
         return new Retrofit.Builder()
                 .baseUrl(BuildConfig.FIREBASE_DB)
                 .addConverterFactory(MoshiConverterFactory.create(moshi))
                 .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                 .build();
    }

    @ActivityScoped
    @Provides
    static Moshi provideMoshi(){
        return new Moshi.Builder().build();
    }
}
