package com.dreamyazilim.TypeTest.ui.Market;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.data.items.MarketItems;

import java.util.List;


public class MarketAdapter extends RecyclerView.Adapter<MarketAdapter.MarketViewHolder> {

    private ImageView imageViewItemPicture;
    private TextView textViewTitle, textViewDescription, textViewPrice;

    private List<MarketItems> marketItem;

    public class MarketViewHolder extends RecyclerView.ViewHolder{

        public MarketViewHolder(View itemView) {
            super(itemView);
            imageViewItemPicture = (ImageView)itemView.findViewById(R.id.imageViewItemPicture);
            textViewTitle = (TextView)itemView.findViewById(R.id.textViewTitle);
            textViewDescription = (TextView)itemView.findViewById(R.id.textViewDescription);
            textViewPrice = (TextView)itemView.findViewById(R.id.textViewPrice);
        }
    }

    public MarketAdapter(List<MarketItems> marketItem){
        this.marketItem = marketItem;
    }

    @NonNull
    @Override
    public MarketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.market_items_row, parent, false);

        return new MarketViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MarketViewHolder holder, int position) {
        MarketItems item = marketItem.get(position);
        imageViewItemPicture.setImageResource(item.getItemPicture());
        textViewTitle.setText(item.getTitle());
        textViewDescription.setText(item.getDescription());
        textViewPrice.setText(item.getPrice());
    }

    @Override
    public int getItemCount() {
        return marketItem.size();
    }


}
