package com.dreamyazilim.TypeTest.ui.UserDetails;

import android.net.Network;

import com.dreamyazilim.TypeTest.data.BuildConfig;
import com.dreamyazilim.TypeTest.data.model.UserDetailsModel;
import com.dreamyazilim.TypeTest.di.ActivityScoped;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.Moshi;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import okhttp3.CertificatePinner;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public abstract class UserDetailsModule {

    @ActivityScoped
    @Provides
    static UserDetailsContract.Presenter provideUserDetailsPresenter(UserDetailsContract.View userDetailsView, Retrofit retrofit) {
        return new UserDetailsPresenterImpl(userDetailsView,retrofit);
    }

    @ActivityScoped
    @Binds
    abstract UserDetailsContract.View provideUserDetailsView(UserDetailsActivity userDetailsActivity);

    @ActivityScoped
    @Provides
    static Retrofit provideRetrofit(Moshi moshi,OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.FIREBASE_DB)
                .client(okHttpClient)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @ActivityScoped
    @Provides
    static Moshi provideMoshi() {
        final JsonAdapter.Factory LENIENT_FACTORY = new JsonAdapter.Factory() {
            @Override
            public JsonAdapter<?> create(Type type, Set<? extends Annotation> annotations, Moshi moshi) {
                return moshi.nextAdapter(this, type, annotations).lenient();
            }
        };
        return new Moshi.Builder()
                .add(LENIENT_FACTORY)
                .build();
    }

    @ActivityScoped
    @Provides
    static OkHttpClient provideOkHttpClient(Interceptor interceptor){
        return new OkHttpClient.Builder()
                .connectTimeout(4000,TimeUnit.MILLISECONDS)
                .readTimeout(4000,TimeUnit.MILLISECONDS)
                .writeTimeout(4000, TimeUnit.MILLISECONDS)
/*
                .addInterceptor(interceptor)
*/
                .build();
    }
    
    @ActivityScoped
    @Provides
    static Interceptor provideInterceptor(){
        return chain -> {
            Request request = chain.request();
            return chain.proceed(request);
        };
    }

}
