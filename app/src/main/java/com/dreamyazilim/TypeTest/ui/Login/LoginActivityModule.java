package com.dreamyazilim.TypeTest.ui.Login;

import com.dreamyazilim.TypeTest.data.BuildConfig;
import com.dreamyazilim.TypeTest.di.ActivityScoped;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public abstract class LoginActivityModule {

    @ActivityScoped
    @Provides
    static LoginContract.Presenter provideLoginPresenter(LoginContract.View loginView,
                                                         @Named("retrofitNormal")Retrofit retrofit,
                                                         @Named("retrofitForDatabase")Retrofit retrofitForDatabase) {
        return new LoginPresenterImpl(loginView,retrofit,retrofitForDatabase);
    }

    @ActivityScoped
    @Binds
    abstract LoginContract.View provideLoginView(LoginActivity loginActivity);

    @ActivityScoped
    @Provides
    @Named("retrofitNormal")
    static Retrofit provideRetrofit(){
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.FIREBASE_APP)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @ActivityScoped
    @Provides
    @Named("retrofitForDatabase")
    static Retrofit provideRetrofitForDb(Gson gson){
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.FIREBASE_DB)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @ActivityScoped
    @Provides
    static Gson provideGson(){
        return new GsonBuilder()
                .setLenient()
                .create();
    }
}

