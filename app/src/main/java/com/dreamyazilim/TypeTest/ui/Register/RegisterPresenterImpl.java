package com.dreamyazilim.TypeTest.ui.Register;

import android.util.Log;
import com.dreamyazilim.TypeTest.data.model.UserModel;
import com.dreamyazilim.TypeTest.data.remote.FirebaseApi;
import com.dreamyazilim.TypeTest.di.ActivityScoped;

import com.dreamyazilim.TypeTest.ui.UserDetails.UserDetailsActivity;

import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@ActivityScoped
public class RegisterPresenterImpl implements RegisterContract.Presenter {

    private static final String TAG = "RegisterPresenterImpl";

    private Retrofit retrofit;
    private Retrofit retrofitForDatabase;
    private RegisterContract.View view;
    private String localId;
    private String idTokenForVerification;

    @Inject
    public RegisterPresenterImpl(RegisterContract.View view,Retrofit retrofit,Retrofit retrofitForDatabase){
        this.view=view;
        this.retrofit = retrofit;
        this.retrofitForDatabase=retrofitForDatabase;
    }

    //Todo add Progress Dialog
    //If the given conditions supplied, calls createFirebaseUser method
    @Override
    public void attemptRegistration(String userEmail, String userPassword) {
        if (isEmailValid(userEmail)) {
            FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);
            firebaseApi.sendUserCredentialsForRegister(userEmail, userPassword, true)
                    .enqueue(new Callback<UserModel>() {
                        @Override
                        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                            if (response.body()!=null) {
                                idTokenForVerification = response.body().getIdToken();
                                sendEmailVerification(idTokenForVerification);
                                localId=response.body().getLocalId();
                            }
                            view.hideProgressDialog(true);
                        }

                        @Override
                        public void onFailure(Call<UserModel> call, Throwable t) {
                            Log.d(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");
                            view.hideProgressDialog(false);
                        }
                    });
        }

    }

    //Checks if email is valid
    @Override
    public boolean isEmailValid(String userEmail) {
        return userEmail.contains("@");
    }

    //Sends verification request to users email.
    @Override
    public void sendEmailVerification(String idToken) {
        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);
        firebaseApi.sendEmailVerification("VERIFY_EMAIL",idToken)
                .enqueue(new Callback<UserModel>() {
                    @Override
                    public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                        view.isVerificationSent(true);
                        view.navigateToActivity(UserDetailsActivity.class);
                    }

                    @Override
                    public void onFailure(Call<UserModel> call, Throwable t) {
                        view.isVerificationSent(false);
                        Log.d(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");
                    }
                });

    }

}
