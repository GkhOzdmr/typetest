package com.dreamyazilim.TypeTest.ui.UserDetails;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.util.StringUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserDetailsActivity extends DaggerAppCompatActivity implements UserDetailsContract.View {

    private static final String TAG = "UserDetailsActivity";
    
    @Inject
    UserDetailsContract.Presenter userDetailsPresenter;
    @BindView(R.id.eTextDisplayName) EditText eTextDisplayName;
    @BindView(R.id.iViewUserPhoto) CircleImageView iViewUserPhoto;

    private ProgressDialog progressDialog;

    private Uri filePath;
    private String photoUrl="https://firebasestorage.googleapis.com/v0/b/typetest-36286.appspot.com/o/user-profile-photos%2Ftest-user%2Fprofile_default.png?alt=media&token=60366a48-2d97-4e3d-9474-05ddd3f1aa24";
    private static final int SELECT_PICTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        ButterKnife.bind(this);
        userDetailsPresenter.uploadImage();
    }

    @Override
    protected void onDestroy() {
        userDetailsPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setImage(resultCode,requestCode,data);
    }

    @OnClick(R.id.buttonConfirm)
    public void onButtonConfirmClick() {

        filePath = Uri.parse(photoUrl);

        String userName = eTextDisplayName.getText().toString().replaceAll("\\s+", "");

        if (userName.length() > 6 && userName.length() < 14) {

            if (!StringUtils.containsAny(userName, StringUtils.specialChars())) {

                userDetailsPresenter.checkIfUserNameExists(userName, photoUrl);
/*
                userDetailsPresenter.setUserDetails(userName, photoUrl);
*/

            } else {

                eTextDisplayName.setError(getResources().getString(R.string.username_special_char));
            }

        } else {

            eTextDisplayName.setError(getResources().getString(R.string.username_lenght));
        }
    }

    @OnClick(R.id.iViewUserPhoto)
    public void onIViewUserPhotoClick(){
        getImage();
    }

    @Override
    public void getImage() {
        //Opens intent for getting image
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    public void setImage(int resultCode,int requestCode,Intent data) {
        //Gets the image data from intent and passes it to Uri filePath
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                this.filePath = data.getData();
                Log.d(TAG, "filePath: "+ filePath);
                Glide.with(this)
                        .load(filePath)
                        .apply(new RequestOptions().override(300,300))
                        .apply(RequestOptions.fitCenterTransform())
                        .into(iViewUserPhoto);
            }
        }
    }

    @Override
    public void showToast(int displayMessage, @Nullable String message) {
        switch (displayMessage){
            case 0:
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                break;
            case 1: //Todo strings.xml
                Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(this, "Failure", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void navigateToActivity(Class<?> cls) {
        startActivity(new Intent(UserDetailsActivity.this,cls));
        finish();
    }

    @Override
    public void showProggressDialog(String message) {
        progressDialog = new ProgressDialog(this,R.style.ProgressDialogStyleGreen);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    @Override
    public void hideProggressDialog() {
        progressDialog.hide();
        progressDialog.dismiss();
    }
}
