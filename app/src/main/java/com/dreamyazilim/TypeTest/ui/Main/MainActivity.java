package com.dreamyazilim.TypeTest.ui.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.ui.Login.LoginActivity;
import com.dreamyazilim.TypeTest.ui.Register.RegisterActivity;
import com.dreamyazilim.TypeTest.ui.SpeedTest.SpeedTestActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private long backPressedTime;
    private Toast backToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        runFadeInAnimation();
    }

    @OnClick(R.id.buttonStartTest)
    public void onButtonStartTestClick(){
       startActivity(new Intent(MainActivity.this, SpeedTestActivity.class));
    }

    @OnClick(R.id.buttonSignIn)
    public void onButtonSignInClick(){
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }

    @OnClick(R.id.buttonSignUp)
    public void onButtonSignUpClick(){
        startActivity(new Intent(MainActivity.this, RegisterActivity.class));
    }

    private void runFadeInAnimation() {
        Animation a = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        a.reset();
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativeLayoutMain);
        rl.clearAnimation();
        rl.startAnimation(a);
    }

    @Override
    public void onBackPressed() {
        //Exits the app when user presses Back button two times in 2 seconds
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(homeIntent);
            super.onBackPressed();
            return;
        } else {
            backToast = Toast.makeText(this,
                    getBaseContext().getResources().getString(R.string.press_back_again),
                    Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }
}
