package com.dreamyazilim.TypeTest.ui.Profile;

import android.util.Log;

import com.dreamyazilim.TypeTest.data.model.UserDetailsModel;
import com.dreamyazilim.TypeTest.data.remote.FirebaseApi;
import com.dreamyazilim.TypeTest.ui.Login.LoginPresenterImpl;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
import retrofit2.Retrofit;

public class ProfilePresenterImpl implements ProfileContract.Presenter {

    private static final String TAG = "ProfilePresenterImpl";

    private ProfileContract.View profileView;
    private Retrofit retrofit;

    @Inject
    public ProfilePresenterImpl(ProfileContract.View profileView, Retrofit retrofit) {
        this.profileView = profileView;
        this.retrofit = retrofit;
    }

    @Override
    public void getUserDetails() {

        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);

        firebaseApi.getUserDetails(LoginPresenterImpl.localId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<UserDetailsModel.UserDetails>() {
                    @Override
                    public void onNext(UserDetailsModel.UserDetails userDetails) {
                      profileView.updateUi(userDetails.getDisplayName(),userDetails.getPhotoUrl());
                    }

                    @Override
                    public void onError(Throwable e) {
                       if(e instanceof HttpException){
                           Log.d(TAG, "onError of getUserDetails " +
                                   "code:"+((HttpException) e).code()+
                                   " response "+((HttpException) e).response()+
                                   " message "+((HttpException) e).message());
                       }else{
                           Log.d(TAG, "onError of getUserDetails " +
                                   "localMessage:"+e.getLocalizedMessage()+
                                   " message "+e.getMessage()+
                                   " cause "+e.getCause());
                       }
                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }
}
