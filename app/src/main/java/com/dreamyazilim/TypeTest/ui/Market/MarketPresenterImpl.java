package com.dreamyazilim.TypeTest.ui.Market;

import com.dreamyazilim.TypeTest.di.ActivityScoped;

import javax.inject.Inject;

@ActivityScoped
public class MarketPresenterImpl implements MarketContract.Presenter {

    private MarketContract.View view;

    @Inject
    public MarketPresenterImpl(MarketContract.View view){
        this.view = view;
    }
}
