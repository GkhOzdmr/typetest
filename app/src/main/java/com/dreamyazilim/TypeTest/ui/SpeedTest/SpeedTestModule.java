package com.dreamyazilim.TypeTest.ui.SpeedTest;

import android.content.Context;

import com.dreamyazilim.TypeTest.di.ActivityScoped;
import com.dreamyazilim.TypeTest.util.ViewUtils;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class SpeedTestModule {

    @ActivityScoped
    @Provides
    static SpeedTestContractor.Presenter speedTestPresenter(SpeedTestContractor.View speedTestView){
        return new SpeedTestPresenterImpl(speedTestView);
    }

    @ActivityScoped
    @Binds
    abstract SpeedTestContractor.View provideSpeedTestView(SpeedTestActivity speedTestActivity);

    @ActivityScoped
    @Provides
    static ViewUtils viewUtils(Context context){
        return new ViewUtils(context);
    }

    @ActivityScoped
    @Provides
    static WpmCalculator wpmCalculator(){
        return new WpmCalculator();
    }
}
