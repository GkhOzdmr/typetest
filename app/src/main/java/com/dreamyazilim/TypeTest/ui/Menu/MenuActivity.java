package com.dreamyazilim.TypeTest.ui.Menu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.ui.Profile.ProfileActivity;
import com.dreamyazilim.TypeTest.ui.SpeedTest.SpeedTestActivity;
import com.dreamyazilim.TypeTest.ui.Market.MarketActivity;
import com.dreamyazilim.TypeTest.ui.Statistics.StatisticsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuActivity extends AppCompatActivity {

    @BindView(R.id.buttonTypeTest) Button buttonTypeTest;
    @BindView(R.id.buttonStatistics) Button buttonStatistics;
    /*@BindView(R.id.buttonMarket) Button buttonMarket;*/
    @BindView(R.id.buttonProfile) Button buttonProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonTypeTest)
    public void onButtonTypeTestClick(){
        startActivity(new Intent(MenuActivity.this, SpeedTestActivity.class));
    }
    
   /* @OnClick(R.id.buttonMarket)
    public void onButtonMarketClick(){
        startActivity(new Intent(MenuActivity.this, MarketActivity.class));
    }*/

    @OnClick(R.id.buttonStatistics)
    public void onButtonStatisticsClick(){
        startActivity(new Intent(MenuActivity.this, StatisticsActivity.class));
    }

    @OnClick(R.id.buttonProfile)
    public void onButtonProfileClick(){
        startActivity(new Intent(MenuActivity.this, ProfileActivity.class));
    }

}
