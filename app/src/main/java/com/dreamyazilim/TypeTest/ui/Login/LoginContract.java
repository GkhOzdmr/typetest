package com.dreamyazilim.TypeTest.ui.Login;

import android.content.Intent;

import com.dreamyazilim.TypeTest.BasePresenter;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public interface LoginContract {

    interface View{

        void showToast(String message);

        void navigateToActivity(Class<?> cls);

        void showProgresDialog(String message);

        void hideProgresDialog();

        void doGoogleSignIn(int requestCode, Intent data);

        void googleSignInOptions();

        void openGoogleAuthScreen();

        void showCodeSentInfo(boolean isSent);

    }

    interface Presenter extends BasePresenter<View> {

        void attemptPasswordLogin(String email, String password);

        void loginGoogleSignIn(GoogleSignInAccount acct);

        void loginFacebookSignIn();

        void isEmailVerified(String idToken);

        void sendResetCode(String email);

        void isDisplayNameDeclared(String localId);

    }
}
