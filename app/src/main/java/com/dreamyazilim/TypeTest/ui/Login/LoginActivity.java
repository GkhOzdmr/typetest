 package com.dreamyazilim.TypeTest.ui.Login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.dreamyazilim.TypeTest.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.dreamyazilim.TypeTest.ui.Profile.ProfileActivity;
import com.dreamyazilim.TypeTest.ui.Register.RegisterActivity;


import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;


public class LoginActivity extends DaggerAppCompatActivity implements LoginContract.View {

    @Inject
    LoginContract.Presenter loginPresenter;

    @BindView(R.id.rLayoutLogin) RelativeLayout rLayoutLogin;
    @BindView(R.id.editTextEmail) EditText editTextEmail;
    @BindView(R.id.editTextPassword) EditText editTextPassword;
    @BindView(R.id.buttonLogin) Button buttonLogin;
    @BindString(R.string.send_reset_code) String send_reset_code;
    @BindString(R.string.we_couldnt_sent_code) String we_couldnt_sent_code;
    //Constants
    private static final String TAG = "LoginActivity";
    private static final int RC_SIGN_IN = 1;

    PopupWindow popupWindow;
    String codeSentInfo;
    AlertDialog.Builder alertDialog;

    private GoogleSignInClient mGoogleSignInClient;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate: ");
        //Runs animation
        runFadeInAnimation();
        //Init AlertDialog
        alertDialog = new AlertDialog.Builder(this,R.style.ProgressDialogStyleGreen);
        //Init ProgressDialog
        mProgressDialog = new ProgressDialog(this,R.style.ProgressDialogStyleGreen);

        //Todo implement google sign in into presenter
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        doGoogleSignIn(requestCode, data);
    }

    //Click listener for buttonRegister
    @OnClick(R.id.textViewRegister)
    public void textViewRegisterClick() {
        Log.d(TAG, "onTextViewRegisterClick:");
        //Switches to RegisterActivity
        navigateToActivity(RegisterActivity.class);
    }

    @OnClick(R.id.buttonLogin)
    public void onLoginButtonClick(){
        String email=editTextEmail.getText().toString(),
                password=editTextPassword.getText().toString();
        loginPresenter.attemptPasswordLogin(email,password);
    }

    @OnClick(R.id.buttonGoogleSignIn)
    public void onGoogleSignInClick() {
        googleSignInOptions();
        //Sends intent call to onActivityResult to open
        //GMail choosing screen to use Google SignUp method
        openGoogleAuthScreen();
    }

    @OnClick(R.id.textViewForgotPassword)
    public void onTViewForgotPasswordClick(){
        editTextEmail.clearFocus();
        editTextPassword.clearFocus();
        buttonLogin.clearFocus();
        editTextEmail.setEnabled(false);
        editTextPassword.setEnabled(false);
        buttonLogin.setEnabled(false);
        LayoutInflater inflater = (LayoutInflater) getApplicationContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.password_reset_popup_layout,null);
        popupWindow = new PopupWindow(customView,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);
        popupWindow.showAtLocation(rLayoutLogin, Gravity.CENTER,0,0);
        popupWindow.setFocusable(true);

        RelativeLayout rLayoutCode = (RelativeLayout) customView.findViewById(R.id.rLayoutCode);
        Button sendCodeButton = (Button) customView.findViewById(R.id.buttonSendCode);
        EditText eTextEmailAddress= (EditText) customView.findViewById(R.id.eTextRstPasEmail);
        Button closeButton = (Button) customView.findViewById(R.id.buttonPopupClose);
        
        sendCodeButton.setOnClickListener(v -> {
            String email = eTextEmailAddress.getText().toString().replaceAll("\\s+", "");
            if(email.contains("@")){
                loginPresenter.sendResetCode(email);
            }else {
                showCodeSentInfo(false);
            }

        });

        closeButton.setOnClickListener(v -> {
                    popupWindow.dismiss();
                    editTextEmail.setEnabled(true);
                    editTextPassword.setEnabled(true);
                    buttonLogin.setEnabled(true);
                });
        popupWindow.update();
    }

    @Override
    public void showCodeSentInfo(boolean isSent) {
        if(isSent){
            codeSentInfo = send_reset_code;
            alertDialog
                    .setTitle("")
                    .setMessage(getResources().getString(R.string.we_sent_code))
                    .setNegativeButton(getResources().getString(R.string.okey), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            popupWindow.dismiss();
                            editTextEmail.setEnabled(true);
                            editTextPassword.setEnabled(true);
                            buttonLogin.setEnabled(true);
                        }
                    }).show();
        }
        else {
            codeSentInfo = we_couldnt_sent_code;
            alertDialog
                    .setTitle("")
                    .setMessage(getResources().getString(R.string.we_couldnt_sent_code))
                    .setNegativeButton(getResources().getString(R.string.okey), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            popupWindow.dismiss();
                            editTextEmail.setEnabled(true);
                            editTextPassword.setEnabled(true);
                            buttonLogin.setEnabled(true);
                        }
                    }).show();
        }
    }

    //Fadeout animation for screen changes
    private void runFadeInAnimation() {
        //fade animation
        Animation fadeAnim = AnimationUtils.loadAnimation(this, R.anim.fadeout);
        fadeAnim.reset();
        //specify the layout to implement animation here
        rLayoutLogin.clearAnimation();
        rLayoutLogin.startAnimation(fadeAnim);
    }


    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToActivity(Class<?> cls) {
        Context context = getBaseContext();
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
        finish();
    }

    @Override
    public void showProgresDialog(String message) {
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    public void hideProgresDialog() {
        mProgressDialog.hide();
        mProgressDialog.dismiss();
    }

    @Override
    public void doGoogleSignIn(int requestCode,Intent data) {
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                loginPresenter.loginGoogleSignIn(account);
                //TODO Change ProfileActivity to MenuActivity
                navigateToActivity(ProfileActivity.class);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed: exception: ", e);
                showToast("Failed to sign in with Google");
            }
        }
    }

    @Override
    public void googleSignInOptions() {
        //Todo implement google sign in into presenter
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void openGoogleAuthScreen() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}

