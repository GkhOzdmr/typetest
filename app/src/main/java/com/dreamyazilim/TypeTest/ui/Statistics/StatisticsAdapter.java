package com.dreamyazilim.TypeTest.ui.Statistics;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.data.items.StatisticsItems;

import java.util.List;


public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.StatisticsViewHolder> {

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }

    private ImageView imageViewItemPicture;
    private TextView textViewTitle, textViewDescription;
    private RelativeLayout rLayoutMarket;

    private List<StatisticsItems> statisticsItems;

    public class StatisticsViewHolder extends RecyclerView.ViewHolder{

        public StatisticsViewHolder(View itemView,OnItemClickListener listener) {
            super(itemView);
            imageViewItemPicture = (ImageView)itemView.findViewById(R.id.imageViewItemPicture);
            textViewTitle = (TextView)itemView.findViewById(R.id.textViewTitle);
            textViewDescription = (TextView)itemView.findViewById(R.id.textViewDescription);
            rLayoutMarket = (RelativeLayout)itemView.findViewById(R.id.rLayoutMarket);

            itemView.setOnClickListener(v -> {
                if(listener!=null){
                   int position = getAdapterPosition();
                   if(position!=RecyclerView.NO_POSITION){
                       onItemClickListener.onItemClick(position);
                   }
                }
            });
        }
    }

    public StatisticsAdapter(List<StatisticsItems> statisticsItems){
        this.statisticsItems = statisticsItems;
    }

    @NonNull
    @Override
    public StatisticsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.statistics_items_row, parent, false);

        return new StatisticsViewHolder(itemView,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull StatisticsViewHolder holder, int position) {
        StatisticsItems item = statisticsItems.get(position);
        imageViewItemPicture.setImageResource(item.getPictureId());
        textViewTitle.setText(item.getTitle());
        textViewDescription.setText(item.getDescription());

    }

    @Override
    public int getItemCount() {
        return statisticsItems.size();
    }

}
