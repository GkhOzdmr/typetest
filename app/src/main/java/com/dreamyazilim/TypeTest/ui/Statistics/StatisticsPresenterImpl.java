package com.dreamyazilim.TypeTest.ui.Statistics;


import com.dreamyazilim.TypeTest.di.ActivityScoped;

import javax.inject.Inject;

@ActivityScoped
public class StatisticsPresenterImpl implements StatisticsContract.Presenter {

    StatisticsContract.View view;

    @Inject
    public StatisticsPresenterImpl(StatisticsContract.View view){
        this.view = view;
    }

}
