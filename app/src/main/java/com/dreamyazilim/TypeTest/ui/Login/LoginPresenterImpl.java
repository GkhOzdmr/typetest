package com.dreamyazilim.TypeTest.ui.Login;

import android.util.Log;

import com.dreamyazilim.TypeTest.data.model.UserDetailsModel;
import com.dreamyazilim.TypeTest.data.model.UserModel;
import com.dreamyazilim.TypeTest.data.remote.FirebaseApi;
import com.dreamyazilim.TypeTest.di.ActivityScoped;
import com.dreamyazilim.TypeTest.ui.Menu.MenuActivity;
import com.dreamyazilim.TypeTest.ui.UserDetails.UserDetailsActivity;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;

@ActivityScoped
public class LoginPresenterImpl implements LoginContract.Presenter {

    private static final String TAG = "LoginPresenterImpl";
    private LoginContract.View view;

    private Retrofit retrofit;
    private Retrofit retrofitForDatabase;

    public static String idToken;
    public static String localId;


    @Inject
    public LoginPresenterImpl(LoginContract.View view,
                              @Named("retrofitNormal") Retrofit retrofit,
                              @Named("retrofitForDatabase") Retrofit retrofitForDatabase) {
        this.view = view;
        this.retrofit = retrofit;
        this.retrofitForDatabase=retrofitForDatabase;
    }


    @Override
    public void attemptPasswordLogin(String email, String password) {
        Log.d(TAG, "buttonSignInClick: onLoginPresenter ");
        view.showProgresDialog("Signing in...");

        retrofit.create(FirebaseApi.class);

        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);

        firebaseApi
                .sendUserCredentialsForLogin(email, password, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<UserModel>() {
                    @Override
                    public void onNext(UserModel user) {
                        if (user.getRegistered().toString().equals("true")) {
                            Log.d(TAG, "onResponse: api call successful");
                            Log.d(TAG, "email: " + user.getEmail());
                            Log.d(TAG, "idToken: " + user.getIdToken());
                            Log.d(TAG, "registered: " + user.getRegistered());
                            Log.d(TAG, "LocalId: " + user.getLocalId());
                            String idToken = user.getIdToken();
                            //Checks the email verification with the token
                            isEmailVerified(idToken);
                            view.hideProgresDialog();
                        } else {
                            Log.d(TAG, "user not registered");
                            view.hideProgresDialog();
                            view.showToast("Failed to sign in");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError: e: " + e.getStackTrace());
                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }

    @Override
    public void isEmailVerified(String idToken) {
        Log.d(TAG, "isEmailVerified: String idToken at start: " + idToken);
        /* Send email verification if
        the user's email is not verified */
        retrofit.create(FirebaseApi.class);

        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);

        firebaseApi.isEmailValid(idToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<UserDetailsModel>() {
                    @Override
                    public void onNext(UserDetailsModel userDetailsModel) {
                        ArrayList<UserDetailsModel.User> user = new ArrayList<>(userDetailsModel.getUsers());
                        for (UserDetailsModel.User u : user) {
                            Log.d(TAG, "localId: " + u.getLocalId().toString());
                            localId=u.getLocalId();
                            if (u.isEmailVerified()) {
                                isDisplayNameDeclared(localId);
                            } else{
                                Log.d(TAG, "onNext: noDisplayName: "+u.getDisplayName());
                                view.showToast("Email is not verified");//Todo handgle hardcoded string
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError of isEmailVerified e: " + e.getStackTrace());
                        if(e instanceof HttpException){
                            Response<?> response = ((HttpException) e).response();
                            String message = ((HttpException) e).message();
                            int code = ((HttpException) e).code();
                            Log.d(TAG, "onError: of getUserDetails: " +
                                    "response: "+response
                                    +" message: "+ message
                                    +" code: "+code);
                        }
                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }

    @Override
    public void sendResetCode(String email) {
        view.showProgresDialog("In progress...");
        FirebaseApi firebaseApi = retrofit.create(FirebaseApi.class);
        firebaseApi.sendResetcode("PASSWORD_RESET", email)
                .enqueue(new Callback<UserModel.ResetCode>() {
                    @Override
                    public void onResponse(Call<UserModel.ResetCode> call, Response<UserModel.ResetCode> response) {
                        view.showCodeSentInfo(true);
                        view.hideProgresDialog();
                    }

                    @Override
                    public void onFailure(Call<UserModel.ResetCode> call, Throwable t) {
                        view.showCodeSentInfo(false);
                        view.hideProgresDialog();
                    }
                });
    }

    @Override
    public void loginGoogleSignIn(GoogleSignInAccount acct) {

    }


    @Override
    public void loginFacebookSignIn() {

    }

    @Override
    public void isDisplayNameDeclared(String localId) {
        Log.d(TAG, "isDisplayNameDeclared: ");
        FirebaseApi firebaseApi = retrofitForDatabase.create(FirebaseApi.class);
        firebaseApi.getUserDetails(localId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<UserDetailsModel.UserDetails>() {

                    @Override
                    public void onNext(UserDetailsModel.UserDetails userDetails) {
                        Response<UserDetailsModel.UserDetails> response = Response.success(userDetails);
                        if(response.body()!=null||userDetails.getDisplayName()!=null || !userDetails.getDisplayName().equals("")){
                            view.navigateToActivity(MenuActivity.class);
                        }else{
                            view.navigateToActivity(UserDetailsActivity.class);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError of isDisplayNameDeclared e: " + e.getMessage()+" "+e.getLocalizedMessage());
                        if(e.getLocalizedMessage().equals("Null is not a valid element")){
                            view.navigateToActivity(UserDetailsActivity.class);
                        }
                        if(e instanceof HttpException){
                            Response<?> response = ((HttpException) e).response();
                            String message = ((HttpException) e).message();
                            int code = ((HttpException) e).code();
                            Log.d(TAG, "onError: of createUserInDatabase: " +
                                    "response: "+response
                                    +" message: "+ message
                                    +" code: "+code);
                        }else{
                            Log.d(TAG, "onError: of createUserInDatabase: " + e.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {
                        dispose();
                    }
                });
    }

    @Override
    public void attach(LoginContract.View view) {

    }

    @Override
    public void detach() {

    }

}
