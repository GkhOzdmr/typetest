package com.dreamyazilim.TypeTest.ui.Profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dreamyazilim.TypeTest.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends DaggerAppCompatActivity implements ProfileContract.View{

    private static final String TAG = "ProfileActivity";
    //For profile image - return to onActivityResult
    private static final int SELECT_PICTURE = 1;

    @Inject
    ProfileContract.Presenter profilePresenter;

    //View Bindings with ButterKnife
    @BindView(R.id.buttonBack) Button buttonBack;
    @BindView(R.id.relativeLayoutProfile) RelativeLayout relativeLayoutProfile;
    @BindView(R.id.imageViewUserPicture) CircleImageView imageViewUserPicture;
    @BindView(R.id.imageViewAddPicture) ImageView imageViewAddPicture;
    @BindView(R.id.eTextChangeUserName) EditText eTextChangeUserName;
    @BindView(R.id.eTextChangePassword) EditText eTextChangePassword;
    @BindView(R.id.textViewUserName) TextView textViewUserName;

    //For profile image
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        profilePresenter.getUserDetails();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setImage(resultCode,requestCode,data);
    }

    @OnClick(R.id.buttonBack)
    public void onButtonBackClick(){
        Log.d(TAG, "buttonBackClick: ");
        super.onBackPressed();
    }

    @OnClick({R.id.imageViewAddPicture, R.id.imageViewUserPicture})
    public void imageViewAddPictureClick(){
        //Will open an intent for choosing an image/photo
        getImage();
    }

    @OnClick(R.id.buttonChangeUserName)
    public void onButtonChangeUserNameClick(){
       eTextChangeUserName.setEnabled(true);
       eTextChangeUserName.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(eTextChangeUserName, InputMethodManager.SHOW_IMPLICIT);
    }

    @OnClick(R.id.buttonChangePassword)
    public void onButtonChangePasswordClick(){
        eTextChangePassword.setEnabled(true);
        eTextChangePassword.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(eTextChangePassword, InputMethodManager.SHOW_IMPLICIT);
    }

    @OnClick(R.id.buttonApplyChanges)
    public void onButtonApplyChangesClick(){
    }

    @Override
    public void updateUi(String displayName,String photoUrl) {
        eTextChangeUserName.setHint(displayName);
        textViewUserName.setText(displayName);
        filePath = Uri.parse(photoUrl);
        Glide.with(this)
               .load(filePath)
               .apply(new RequestOptions().override(300,300))
               .apply(RequestOptions.fitCenterTransform())
               .into(imageViewUserPicture);
    }

    //Gets the image from gallery
    private Intent getImage(){
        //Opens intent for getting image
        //Passes info to onActivityResult to make other things
        //such as uploading image or displaying it
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        /*intent.setType("image/*");
        intent.setAction(Intent.CATEGORY_APP_GALLERY);*/
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        return intent;
    }

    //Sets the picked image into imageViewUserPhoto
    private boolean setImage(int resultCode,int requestCode,Intent data){
        //Gets the image data from intent and passes it to Uri filePath
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {

                this.filePath = data.getData();

                Glide.with(this)
                        .load(filePath)
                        .apply(new RequestOptions().override(300,300))
                        .apply(RequestOptions.fitCenterTransform())
                        .into(imageViewUserPicture);

                return true;
            }
        }
        return false;
    }

}
