package com.dreamyazilim.TypeTest.ui.SpeedTest;

public class PointsCalculator {

    private static int points;

    public static int calculatePointsEarned(int wpm){

        if(wpm<=30){points = 2;}
        else if(wpm<=40){points = 3;}
        else if(wpm<=45){points = 4;}
        else if(wpm<=50){points = 5;}
        else if(wpm<=60){points = 7;}
        else if(wpm<=70){points = 10;}
        else if(wpm<=80){points = 15;}
        else if(wpm<=90){points = 20;}
        return points;

    }
}
