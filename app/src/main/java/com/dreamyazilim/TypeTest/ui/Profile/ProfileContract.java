package com.dreamyazilim.TypeTest.ui.Profile;

public interface ProfileContract {

    interface View{
      void updateUi(String displayName,String photoUrl);
    }

    interface Presenter{
      void getUserDetails();
    }
}
