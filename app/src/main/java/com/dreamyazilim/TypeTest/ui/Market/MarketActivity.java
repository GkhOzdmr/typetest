package com.dreamyazilim.TypeTest.ui.Market;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;

import com.dreamyazilim.TypeTest.R;
import com.dreamyazilim.TypeTest.data.items.MarketItems;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class MarketActivity extends DaggerAppCompatActivity implements MarketContract.View {

    @Inject
    MarketContract.Presenter marketPresenter;

    @BindView(R.id.drawerLayoutMarket) DrawerLayout drawerLayoutMarket;
    @BindView(R.id.recyclerViewMarket) RecyclerView recyclerViewMarket;

    private List<MarketItems> marketItem = new ArrayList<>();
    private MarketAdapter marketAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market);
        ButterKnife.bind(this);


        marketAdapter = new MarketAdapter(marketItem);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewMarket.setLayoutManager(mLayoutManager);
        recyclerViewMarket.setItemAnimator(new DefaultItemAnimator());
        recyclerViewMarket.setAdapter(marketAdapter);

        MarketItems item = new MarketItems(R.drawable.profile_placeholder,
                "ad-block",
                "block all the ads in the app",
                "5TL");
        marketItem.add(item);
        marketAdapter.notifyDataSetChanged();


    }

    @Override
    public void onBackPressed() {
        //If Navigation Menu is opened already then closes it
        //If its closed, then does the super.onBackPressed
        if(drawerLayoutMarket.isDrawerOpen(Gravity.LEFT)){
            drawerLayoutMarket.closeDrawer(Gravity.LEFT);
        }else{
            super.onBackPressed();
        }
    }

    @OnClick(R.id.buttonNavMenu)
    public void onButtonNavigationMenuClick(){
        //Opens Navigation Drawer to the left on button click
        drawerLayoutMarket.openDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.buttonBack)
    public void onButtonBackClick(){
        super.onBackPressed();
    }

}
