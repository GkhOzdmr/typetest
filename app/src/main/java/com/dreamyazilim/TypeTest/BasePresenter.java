package com.dreamyazilim.TypeTest;

public interface BasePresenter<V> {

    void attach(V view);

    void detach();

}
