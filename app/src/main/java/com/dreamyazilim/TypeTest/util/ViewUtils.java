package com.dreamyazilim.TypeTest.util;

import android.content.Context;

public class ViewUtils {


    Context context;

    public ViewUtils(Context context) {
        this.context = context;
    }

    /**
     *
     * @return Will return the screen width in dp value.
     */
    public float screenWidthInDp(){
        float density  = context.getResources().getDisplayMetrics().density;
        float width = context.getResources().getDisplayMetrics().widthPixels;
        return (width / density);
    }

    public float screenDensity(){
        return context.getResources().getDisplayMetrics().density;
    }

    public int textSizeSetterBig(){
        int textSize;
        if (screenDensity()<=2&&screenDensity()!=0){
            textSize = (int) screenDensity() * 17;
        }else{
            textSize = (int) screenDensity() * 10;
        }
        return textSize;
    }

    public int textSizeSetterSmall(){
        int textSize;
        if (screenDensity()<=2&&screenDensity()!=0){
            textSize = (int) screenDensity() * 13;
        }else{
            textSize = (int) screenDensity() * 7;
        }
        return textSize;
    }

    /**
     *
     * @return Will return the screen width in pixel value.
     */
    public float screenWidthInPx(){
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    /**
     *
     * @param value Pixel value you want to turn into Dp.
     * @return Will return dp value of the specified pixel value.
     */
    public float pixelToDp(float value){
        float density  = context.getResources().getDisplayMetrics().density;
        return (value/density);
    }

    /**
     *
     * @param value Dp value you want to turn into pixels.
     * @return Will return pixelated value of the specified dp value.
     */
    public float dpToPixel(float value) {
        float density = context.getResources().getDisplayMetrics().density;
        return (value * density);
    }

    /**
     *
     * @return Will return one(1) percent of the screen in pixel value.
     */
    public float screenOnePercent(){
        //return (float)(Resources.getSystem().getDisplayMetrics().widthPixels)/100;
        return (float)(context.getResources().getDisplayMetrics().widthPixels)/100;
    }
    /**
     *
     * @return Will return ten(10) percent of the screen in pixel value.
     */
    public float screenTenPercent(){
        return (float)(context.getResources().getDisplayMetrics().widthPixels)/10;
    }
    /**
     *
     * @return Will return twenty five(25) percent of the screen in pixel value.
     */
    public float screenTwentyFivePercent(){
        return (float)(context.getResources().getDisplayMetrics().widthPixels)/25;
    }


}
