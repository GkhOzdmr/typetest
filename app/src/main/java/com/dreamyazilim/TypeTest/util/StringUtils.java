package com.dreamyazilim.TypeTest.util;

public class StringUtils {

    public static char[] specialChars(){
        char[] specialChars = new char[6];
        specialChars[0]='.';
        specialChars[1]='$';
        specialChars[2]='#';
        specialChars[3]='[';
        specialChars[4]=']';
        specialChars[5]='/';
        return specialChars;
    }

    public static boolean containsAny(String str, char[] searchChars) {
        if (str == null || str.length() == 0 || searchChars == null || searchChars.length == 0) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            for (int j = 0; j < searchChars.length; j++) {
                if (searchChars[j] == ch) {
                    return true;
                }
            }
        }
        return false;
    }
}
