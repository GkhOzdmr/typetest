package com.dreamyazilim.TypeTest.di;

import com.dreamyazilim.TypeTest.ui.Login.LoginActivity;
import com.dreamyazilim.TypeTest.ui.Login.LoginActivityModule;
import com.dreamyazilim.TypeTest.ui.Profile.ProfileActivity;
import com.dreamyazilim.TypeTest.ui.Profile.ProfileActivityModule;
import com.dreamyazilim.TypeTest.ui.SpeedTest.SpeedTestActivity;
import com.dreamyazilim.TypeTest.ui.SpeedTest.SpeedTestModule;
import com.dreamyazilim.TypeTest.ui.Market.MarketActivity;
import com.dreamyazilim.TypeTest.ui.Market.MarketActivityModule;
import com.dreamyazilim.TypeTest.ui.Register.RegisterActivity;
import com.dreamyazilim.TypeTest.ui.Register.RegisterActivityModule;
import com.dreamyazilim.TypeTest.ui.Statistics.StatisticsActivity;
import com.dreamyazilim.TypeTest.ui.Statistics.StatisticsActivityModule;
import com.dreamyazilim.TypeTest.ui.UserDetails.UserDetailsActivity;
import com.dreamyazilim.TypeTest.ui.UserDetails.UserDetailsModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ActivityScoped
    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = RegisterActivityModule.class)
    abstract RegisterActivity registerActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = MarketActivityModule.class)
    abstract MarketActivity marketActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = StatisticsActivityModule.class)
    abstract StatisticsActivity statisticsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = SpeedTestModule.class)
    abstract SpeedTestActivity speedTestActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = UserDetailsModule.class)
    abstract UserDetailsActivity userDetailsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ProfileActivityModule.class)
    abstract ProfileActivity profileActivity();
}
