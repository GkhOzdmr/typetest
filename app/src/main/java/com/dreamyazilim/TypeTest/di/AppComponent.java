package com.dreamyazilim.TypeTest.di;

import android.app.Application;

import com.dreamyazilim.TypeTest.MyApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class})
public interface AppComponent extends AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }

}