package com.dreamyazilim.TypeTest.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.dreamyazilim.TypeTest.data.local.WpmRecordDAO;
import com.dreamyazilim.TypeTest.data.model.WpmRecordModel;

@Database(entities = {WpmRecordModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract WpmRecordDAO wpmRecordDAO();

    public static AppDatabase getAppDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context, AppDatabase.class, "typetest_database")
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

}
