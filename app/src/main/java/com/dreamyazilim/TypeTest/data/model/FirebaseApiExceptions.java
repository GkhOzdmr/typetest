package com.dreamyazilim.TypeTest.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/***This class is to handle errors and
 * display/do stuff according to error type***/

public class FirebaseApiExceptions {

    @SerializedName("error")
    private String error;
    @SerializedName("errors")
    private List<Errors> errors=null;
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Errors> getErrors() {
        return errors;
    }

    public void setErrors(List<Errors> errors) {
        this.errors = errors;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Errors {

        @SerializedName("domain")
        private String domain;
        @SerializedName("reason")
        private String reasong;
        @SerializedName("message")
        private String message;

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public String getReasong() {
            return reasong;
        }

        public void setReasong(String reasong) {
            this.reasong = reasong;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
