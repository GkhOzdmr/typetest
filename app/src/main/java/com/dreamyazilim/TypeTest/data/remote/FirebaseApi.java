package com.dreamyazilim.TypeTest.data.remote;

import android.support.annotation.FractionRes;

import com.dreamyazilim.TypeTest.data.model.GoogleOAuthModel;
import com.dreamyazilim.TypeTest.data.BuildConfig;
import com.dreamyazilim.TypeTest.data.model.UserDetailsModel;
import com.dreamyazilim.TypeTest.data.model.UserModel;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

//Do REST calls in this class, We use Retrofit 2 and GSon as tools for it.

public interface FirebaseApi {

    @POST(BuildConfig.FIREBASE_LOGIN)
    @FormUrlEncoded
    Observable<UserModel> sendUserCredentialsForLogin(@Field("email") String email,
                                        @Field("password") String password,
                                        @Field("returnSecureToken") boolean returnSecureToken);
    @POST(BuildConfig.FIREBASE_REGISTER)
    @FormUrlEncoded
    Call<UserModel> sendUserCredentialsForRegister(@Field("email") String email,
                                                   @Field("password") String password,
                                                   @Field("returnSecureToken") boolean returnSecureToken);


    @POST(BuildConfig.FIREBASE_ACCOUNT_INFO)
    @FormUrlEncoded
    Observable<UserDetailsModel> isEmailValid(@Field("idToken") String idToken);

    @POST(BuildConfig.FIREBASE_OAUTH)
    @FormUrlEncoded
    Call<GoogleOAuthModel> sendGoogleCredentials(@Field("requestUri") String requestUri,
                                                 @Field("postBody") String postBody,
                                                 @Field("returnSecureToken") boolean returnSecureToken,
                                                 @Field("returnIdpCredential") boolean returnIdpCredential);

    @POST(BuildConfig.FIREBASE_SEND_VERIFICATION)
    @FormUrlEncoded
    Call<UserModel> sendEmailVerification(@Field("requestType") String requestType,
                                                @Field("idToken") String idToken);

    @POST(BuildConfig.FIREBASE_SEND_RESET_CODE)
    @FormUrlEncoded
    Call<UserModel.ResetCode> sendResetcode(@Field("requestType") String requestType,
                                            @Field("email") String email);

    @GET("users/user-details/{localId}.json")
    Observable<UserDetailsModel.UserDetails> getUserDetails(@Path("localId") String localId);
    
    @PUT("users/user-details/{localId}/details.json")
    Observable<UserDetailsModel.UserDetails> setUserDetails(@Path("localId") String localId,
                                                            @Body UserDetailsModel.UserDetails details);

    @GET("users/user-details.json")
    Single<Response<UserDetailsModel.UserDetails>> checkIfUsernameExists(@Query("orderBy") String queryDisplayName,
                                                                         @Query("equalTo") String displayName);

}
