package com.dreamyazilim.TypeTest.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dreamyazilim.TypeTest.data.model.WpmRecordModel;

import java.util.List;

@Dao
public interface WpmRecordDAO {

    @Query("SELECT * FROM wpmrecordmodel")
    List<WpmRecordModel> getAllRecords();

    @Query("SELECT AVG(curren_wpm_record) FROM wpmrecordmodel")
    int getWpmAverage();

    @Query("SELECT SUM(points_earned) FROM wpmrecordmodel")
    int getTotalPoints();

    @Query("SELECT COUNT(curren_wpm_record) FROM wpmrecordmodel")
    int getTotalTests();

    @Query("SELECT MAX(curren_wpm_record) FROM wpmrecordmodel")
    int getHighestWpm();

    @Insert
    void insertAll(WpmRecordModel wpmRecord);

    @Update
    void update(WpmRecordModel wpmRecord);

    @Delete
    void delete(WpmRecordModel wpmRecord);

}
