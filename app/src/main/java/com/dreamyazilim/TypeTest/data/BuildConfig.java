package com.dreamyazilim.TypeTest.data;

public class BuildConfig {

    private static final String FIREBASE_API_KEY = "";
    public static final String FIREBASE_APP = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/";
    public static final String FIREBASE_DB = "https://typetest-36286.firebaseio.com/";
    public static final String FIREBASE_LOGIN = "verifyPassword?key="+FIREBASE_API_KEY;
    public static final String FIREBASE_REGISTER = "signupNewUser?key="+FIREBASE_API_KEY;
    public static final String FIREBASE_ACCOUNT_INFO = "getAccountInfo?key="+FIREBASE_API_KEY;
    public static final String FIREBASE_OAUTH = "verifyAssertion?key="+FIREBASE_API_KEY;
    public static final String FIREBASE_SEND_VERIFICATION = "getOobConfirmationCode?key="+FIREBASE_API_KEY;
    public static final String FIREBASE_SEND_RESET_CODE = "getOobConfirmationCode?key="+FIREBASE_API_KEY;
    public static final String FIREBASE_DB_USER_DETAILS ="users/user-details/{localId}";
    
}
