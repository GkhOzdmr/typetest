package com.dreamyazilim.TypeTest.data.items;

public class MarketItems {

    int itemPicture;
    String title,description,price;

    public MarketItems(){}

    public MarketItems(int itemPicture,String title,String description,String price){
        this.itemPicture=itemPicture;
        this.title=title;
        this.description=description;
        this.price=price;
    }

    public void setItemPicture(int itemPicture) {
        this.itemPicture = itemPicture;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getItemPicture() {
        return itemPicture;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }
}
