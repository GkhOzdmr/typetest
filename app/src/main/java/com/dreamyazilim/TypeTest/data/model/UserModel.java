package com.dreamyazilim.TypeTest.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("returnSecureToken")
    @Expose
    private boolean returnSecureToken;
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("localId")
    @Expose
    private String localId;
    @SerializedName("displayName")
    @Expose
    private String displayName;
    @SerializedName("idToken")
    @Expose
    private String idToken;
    @SerializedName("registered")
    @Expose
    private String registered;
    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;
    @SerializedName("expiresIn")
    @Expose
    private String expiresIn;
    @SerializedName("emailVerified")
    @Expose
    private boolean emailVerified;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean isReturnSecureToken() {
        return returnSecureToken;
    }

    public String getKind() {
        return kind;
    }

    public String getLocalId() {
        return localId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getIdToken() {
        return idToken;
    }

    public String getRegistered() {
        return registered;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setReturnSecureToken(boolean returnSecureToken) {
        this.returnSecureToken = returnSecureToken;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public class ResetCode{
        @SerializedName("email")
        String email;
        @SerializedName("oobCode")
        String oobCode;
        @SerializedName("newPassword")
        String newPassword;
        @SerializedName("kind")
        String kind;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getOobCode() {
            return oobCode;
        }

        public void setOobCode(String oobCode) {
            this.oobCode = oobCode;
        }

        public String getNewPassword() {
            return newPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }
    }

}
