package com.dreamyazilim.TypeTest.data.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.squareup.moshi.FromJson;
import com.squareup.moshi.Json;
import com.squareup.moshi.ToJson;

import java.util.List;

public class UserDetailsModel {

    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("users")
    @Expose
    private List<User> users = null;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public class User {

        @SerializedName("localId")
        @Expose
        private String localId;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("passwordHash")
        @Expose
        private String passwordHash;
        @SerializedName("emailVerified")
        @Expose
        private boolean emailVerified;
        @SerializedName("displayName")
        @Expose
        private String displayName;
        @SerializedName("passwordUpdatedAt")
        @Expose
        private double passwordUpdatedAt;
        @SerializedName("providerUserInfo")
        @Expose
        private List<ProviderUserInfo> providerUserInfo = null;
        @SerializedName("validSince")
        @Expose
        private String validSince;
        @SerializedName("lastLoginAt")
        @Expose
        private String lastLoginAt;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;

        public String getLocalId() {
            return localId;
        }

        public void setLocalId(String localId) {
            this.localId = localId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPasswordHash() {
            return passwordHash;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public void setPasswordHash(String passwordHash) {
            this.passwordHash = passwordHash;
        }

        public boolean isEmailVerified() {
            return emailVerified;
        }

        public void setEmailVerified(boolean emailVerified) {
            this.emailVerified = emailVerified;
        }

        public double getPasswordUpdatedAt() {
            return passwordUpdatedAt;
        }

        public void setPasswordUpdatedAt(double passwordUpdatedAt) {
            this.passwordUpdatedAt = passwordUpdatedAt;
        }

        public List<ProviderUserInfo> getProviderUserInfo() {
            return providerUserInfo;
        }

        public void setProviderUserInfo(List<ProviderUserInfo> providerUserInfo) {
            this.providerUserInfo = providerUserInfo;
        }

        public String getValidSince() {
            return validSince;
        }

        public void setValidSince(String validSince) {
            this.validSince = validSince;
        }

        public String getLastLoginAt() {
            return lastLoginAt;
        }

        public void setLastLoginAt(String lastLoginAt) {
            this.lastLoginAt = lastLoginAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }
    }

    public class ProviderUserInfo {

        @SerializedName("providerId")
        @Expose
        private String providerId;
        @SerializedName("federatedId")
        @Expose
        private String federatedId;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("rawId")
        @Expose
        private String rawId;

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getFederatedId() {
            return federatedId;
        }

        public void setFederatedId(String federatedId) {
            this.federatedId = federatedId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getRawId() {
            return rawId;
        }

        public void setRawId(String rawId) {
            this.rawId = rawId;
        }

    }
    
    public static class UserDetails{
        @Json(name="localId")
        private String localId;
        @Json(name="details")
        List<Details> details;

        public UserDetails() {
        }

        public UserDetails(String localId, List<Details> details) {
            this.localId = localId;
            this.details = details;
        }

        public String getLocalId() {
            return localId;
        }

        public List<Details> getDetails() {
            return details;
        }

        public void setDetails(List<Details> details) {
            this.details = details;
        }

        public void setLocalId(String localId) {
            this.localId = localId;


        }


       public static class Details{
            @Json(name="displayName")
            private String displayName;
            @Json(name="photoUrl")
            private String photoUrl;

           public Details(String displayName, String photoUrl) {
               this.displayName = displayName;
               this.photoUrl = photoUrl;
           }

           public String getDisplayName() {
                return displayName;
            }

            public void setDisplayName(String displayName) {
                this.displayName = displayName;
            }

            public String getPhotoUrl() {
                return photoUrl;
            }

            public void setPhotoUrl(String photoUrl) {
                this.photoUrl = photoUrl;
            }
        }
    }
}


