package com.dreamyazilim.TypeTest.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity
public class WpmRecordModel {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "date")
    private String date;
    @ColumnInfo(name = "curren_wpm_record")
    private int currentWpmRecord;
    @ColumnInfo(name = "points_earned")
    private int pointsEarned;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCurrentWpmRecord() {
        return currentWpmRecord;
    }

    public void setCurrentWpmRecord(int currentWpmRecord) {
        this.currentWpmRecord = currentWpmRecord;
    }

    public int getPointsEarned() {
        return pointsEarned;
    }

    public void setPointsEarned(int pointsEarned) {
        this.pointsEarned = pointsEarned;
    }
}

